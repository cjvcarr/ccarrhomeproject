package TetrisGameFiles.Resources;

import TetrisApp.TetrisGame;
import TetrisGameFiles.Operations.Controls;
import TetrisGameFiles.Operations.Mechanics;
import TetrisGameFiles.PlayingField.PlayingField;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Created by Christopher Carr on 08/12/2016.
 */
public class NewGame extends Pane {

    public static Stage gameStage = new Stage();

    public static void openGameStage() {
        //Set properties for the game stage window
        gameStage.setTitle("Christopher Carr CSE C11 Tetris");
        Controls.userControls();

        //If the window isn't previously open, create it as a new stage and scene, if it has use current elements
        if (!TetrisGame.playingPaneOpen) {
            Scene playingScene = new Scene(PlayingPane.gamePane, Globals.WIDTH, Globals.HEIGHT, Color.ANTIQUEWHITE);
            gameStage.setScene(playingScene);
            PlayingPane.newGame();
            TetrisGame.playingPaneOpen = true;
        }

        //Setup a new game with Mechanics.setupNewGame
        Mechanics.setupNewGame();
        if (!TetrisGame.gameStageActive) { //If it hasn't been shown yet, show the gameStage
            gameStage.show();
        }
        PlayingField.setScore(0);
        gameStage.setOnCloseRequest(e -> System.exit(0));
    }
}
