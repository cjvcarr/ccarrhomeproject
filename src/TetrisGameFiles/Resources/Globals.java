package TetrisGameFiles.Resources;

import TetrisGameFiles.PlayingField.GridPiece;
import TetrisGameFiles.PlayingField.PlayingField;
import TetrisGameFiles.Tetriminos.Tetrimino;

import java.util.ArrayList;

/**
 * Created by Christopher Carr on 22/11/2016.
 */
public class Globals {

    //Constants for playingField width and height in pixels
    public static final double FIELD_WIDTH = 200;
    public static final double FIELD_HEIGHT = 440;

    //Constants for game window width and height
    public static final double WIDTH = 400;
    public static final double HEIGHT = 500;

    //Constant calculation for offset of playingField offset X and Y inside game window
    public static final double PLAYING_FIELD_X_OFFSET = WIDTH / 2 - 170;
    public static final double PLAYING_FIELD_Y_OFFSET = HEIGHT / 2 - 220;

    //Constant for amount of rows and columns in the grid
    public static final int GRID_ROWS = 22;
    public static final int GRID_COLUMNS = 10;

    //Constants for width and height of the gridpieces
    public static final double GRID_WIDTH = FIELD_WIDTH / GRID_COLUMNS;
    public static final double GRID_HEIGHT = FIELD_HEIGHT / GRID_ROWS;

    //Array of gridPieces that make up the playingField, arrayList of tetriminos generated
    public static GridPiece[][] playingField = PlayingField.generate(Globals.GRID_COLUMNS, Globals.GRID_ROWS,
            Globals.GRID_WIDTH, Globals.GRID_HEIGHT, Globals.PLAYING_FIELD_X_OFFSET, Globals.PLAYING_FIELD_Y_OFFSET);

    public static ArrayList<Tetrimino> tetriminos = Tetrimino.getTetriminos();

}
