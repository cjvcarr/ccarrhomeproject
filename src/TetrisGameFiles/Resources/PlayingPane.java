package TetrisGameFiles.Resources;

import TetrisGameFiles.Operations.Mechanics;
import TetrisGameFiles.PlayingField.PlayingField;
import TetrisGameFiles.Tetriminos.Tetrimino;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Created by Christopher Carr on 07/12/2016.
 */
public class PlayingPane {
    public static Pane gamePane = new Pane();
    public static Text endGameText = new Text(" GAME OVER");
    public static Text scoreText = new Text(" Score: " + PlayingField.getScore());

    //setup the pane when a new game is started from main menu and it isn't previously open.
    public static void newGame() {
        Tetrimino.generateArray(1009); //Generates the array of tetriminos for this game
        for (int i = 0; i < Globals.GRID_COLUMNS; i++) {
            for (int j = 0; j < Globals.GRID_ROWS; j++) {
                //add new node to group
                Globals.playingField[i][j].fillActive(Globals.tetriminos.get(0));
                Globals.playingField[i][j].checkOccupied();
                gamePane.getChildren().add(Globals.playingField[i][j]);
            }
        }
        Mechanics.tickDown(); //Starts the tickdown timer

        //setup buttons in a VBox and add to gamePane
        Button playButton = new Button("New game");
        playButton.setFont(Font.font("Stencil", 15));
        Button quitButton = new Button("Quit");
        quitButton.setFont(Font.font("Stencil", 15));
        Text pauseText = new Text("Press 'p' to pause");
        pauseText.setFont(Font.font(15));
        endGameText.setFont(Font.font("Stencil", 20));
        endGameText.setFill(null);
        scoreText.setFont(Font.font("Stencil", 20));
        VBox vbButtons = new VBox();
        vbButtons.setLayoutX(250);
        vbButtons.setLayoutY(320);
        vbButtons.setSpacing(10);
        vbButtons.getChildren().addAll(endGameText, scoreText, playButton, quitButton, pauseText);
        gamePane.getChildren().addAll(vbButtons);

        playButton.setOnAction(event -> {
            Mechanics.setupNewGame();
        });
        quitButton.setOnAction(e -> System.exit(0));
    }
}
