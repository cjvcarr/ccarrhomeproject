package TetrisGameFiles.Tetriminos;

import TetrisGameFiles.PlayingField.GridPiece;
import TetrisGameFiles.Resources.Globals;
import javafx.scene.paint.Color;

/**
 * Created by Christopher Carr on 31/10/2016.
 */
public class ShapeI extends Tetrimino {
    public ShapeI() {

    }

    private static int iOffset, jOffset;

    public static void drawShape(GridPiece[][] playingField) {
        rotation = 1;
        playingField[3][0].setActive(true);
        playingField[4][0].setActive(true);
        playingField[5][0].setActive(true);
        playingField[6][0].setActive(true);
    }

    public static void setColor() {
        Tetrimino.setTetriminoColor(Color.CYAN);
    }

    public static void rotateShape(GridPiece[][] playingField) {
        //rotation 1-->2
        if (rotation == 1) {
            iOffset = 0;
            jOffset = 0;
            for (int i = 0; i < Globals.GRID_COLUMNS; i++) {
                for (int j = 0; j < Globals.GRID_ROWS; j++) {
                    if (playingField[i][j].isActive()) {
                        if (j == 0 || playingField[i + 2][j - 1].isOccupied()) {
                            jOffset = 1;
                        } else if (j == 21) {
                            jOffset = -2;
                        } else if (playingField[i + 2][j + 1].isOccupied()) {
                            jOffset = -2;
                        } else if (j == 20 || playingField[i + 2][j + 2].isOccupied()) {
                            jOffset = -1;
                        }
                        if (playingField[i + 2 + iOffset][j - 1 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + 2 + iOffset][j + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + 2 + iOffset][j + 1 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + 2 + iOffset][j + 2 + jOffset].isOccupied()) {
                            return;
                        }
                        playingField[i][j].setActive(false);
                        playingField[i + 1][j].setActive(false);
                        playingField[i + 2][j].setActive(false);
                        playingField[i + 3][j].setActive(false);
                        playingField[i + 2 + iOffset][j - 1 + jOffset].setActive(true);
                        playingField[i + 2 + iOffset][j + jOffset].setActive(true);
                        playingField[i + 2 + iOffset][j + 1 + jOffset].setActive(true);
                        playingField[i + 2 + iOffset][j + 2 + jOffset].setActive(true);
                        rotation = 2;
                        return;
                    }
                }
            }
        }

        //rotation 2 --> 3
        if (rotation == 2) {
            iOffset = 0;
            jOffset = 0;
            for (int i = 0; i < Globals.GRID_COLUMNS; i++) {
                for (int j = 0; j < Globals.GRID_ROWS; j++) {
                    if (playingField[i][j].isActive()) {
                        if (i == 0 || playingField[i - 1][j + 2].isOccupied()) {
                            iOffset = 2;
                        } else if (i == 1 || i < 9 && playingField[i + 1][j + 2].isOccupied()) {
                            iOffset = 1;
                        } else if (i == 9 || playingField[i - 2][j + 2].isOccupied()) {
                            iOffset = -1;
                        }
                        if (playingField[i - 2 + iOffset][j + 2 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i - 1 + iOffset][j + 2 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + iOffset][j + 2 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + 1 + iOffset][j + 2 + jOffset].isOccupied()) {
                            return;
                        }
                        playingField[i][j].setActive(false);
                        playingField[i][j + 1].setActive(false);
                        playingField[i][j + 2].setActive(false);
                        playingField[i][j + 3].setActive(false);
                        playingField[i - 2 + iOffset][j + 2 + jOffset].setActive(true);
                        playingField[i - 1 + iOffset][j + 2 + jOffset].setActive(true);
                        playingField[i + iOffset][j + 2 + jOffset].setActive(true);
                        playingField[i + 1 + iOffset][j + 2 + jOffset].setActive(true);
                        rotation = 3;
                        return;
                    }
                }
            }
        }

        //rotation 3-->4
        if (rotation == 3) {
            iOffset = 0;
            jOffset = 0;
            for (int i = 0; i < Globals.GRID_COLUMNS; i++) {
                for (int j = 0; j < Globals.GRID_ROWS; j++) {
                    if (playingField[i][j].isActive()) {
                        if (j == 21 || playingField[i + 1][j + 1].isOccupied()) {
                            jOffset = -1;
                        }
                        if (j == 0 || playingField[i + 1][j - 2].isOccupied()) {
                            jOffset = 1;
                        }
                        if (j == 1 || playingField[i + 1][j - 1].isOccupied()) {
                            jOffset = 2;
                        }
                        if (playingField[i + 1 + iOffset][j - 2 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + 1 + iOffset][j - 1 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + 1 + iOffset][j + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + 1 + iOffset][j + 1 + jOffset].isOccupied()) {
                            return;
                        }
                        playingField[i][j].setActive(false);
                        playingField[i + 1][j].setActive(false);
                        playingField[i + 2][j].setActive(false);
                        playingField[i + 3][j].setActive(false);
                        playingField[i + 1 + iOffset][j - 2 + jOffset].setActive(true);
                        playingField[i + 1 + iOffset][j - 1 + jOffset].setActive(true);
                        playingField[i + 1 + iOffset][j + jOffset].setActive(true);
                        playingField[i + 1 + iOffset][j + 1 + jOffset].setActive(true);
                        rotation = 4;
                        return;
                    }
                }
            }
        }

        //rotation 4-->1
        if (rotation == 4) {
            iOffset = 0;
            jOffset = 0;
            for (int i = 0; i < Globals.GRID_COLUMNS; i++) {
                for (int j = 0; j < Globals.GRID_ROWS; j++) {
                    if (playingField[i][j].isActive()) {
                         if (i == 8 || i < 8 && playingField[i + 2][j + 1].isOccupied()) {
                            iOffset = -1;
                        } else if (i == 9 || playingField[i + 1][j + 1].isOccupied()) {
                            iOffset = -2;
                        } else if (i == 0 || playingField[i - 1][j + 1].isOccupied()) {
                            iOffset = 1;
                        }
                        if (playingField[i - 1 + iOffset][j + 1 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + iOffset][j + 1 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + 1 + iOffset][j + 1 + jOffset].isOccupied()) {
                            return;
                        }
                        if (playingField[i + 2 + iOffset][j + 1 + jOffset].isOccupied()) {
                            return;
                        }
                        playingField[i][j].setActive(false);
                        playingField[i][j + 1].setActive(false);
                        playingField[i][j + 2].setActive(false);
                        playingField[i][j + 3].setActive(false);
                        playingField[i - 1 + iOffset][j + 1 + jOffset].setActive(true);
                        playingField[i + iOffset][j + 1 + jOffset].setActive(true);
                        playingField[i + 1 + iOffset][j + 1 + jOffset].setActive(true);
                        playingField[i + 2 + iOffset][j + 1 + jOffset].setActive(true);
                        rotation = 1;
                        return;
                    }
                }
            }
        }
    }


    @Override
    public String toString() {
        return "ShapeI";
    }

}
