package TetrisGameFiles.Tetriminos;

import TetrisGameFiles.PlayingField.GridPiece;
import javafx.scene.paint.Color;

/**
 * Created by Christopher Carr on 31/10/2016.
 */
public class ShapeO extends Tetrimino {
    public ShapeO() {

    }

    public static void drawShape(GridPiece[][] playingField) {
        playingField[4][0].setActive(true);
        playingField[5][0].setActive(true);
        playingField[4][1].setActive(true);
        playingField[5][1].setActive(true);
    }

    public static void setColor() {
        Tetrimino.setTetriminoColor(Color.YELLOW);
    }

    @Override
    public String toString() {
        return "ShapeO";
    }
}
