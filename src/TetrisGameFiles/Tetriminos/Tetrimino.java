package TetrisGameFiles.Tetriminos;

import TetrisGameFiles.Resources.Globals;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import lib.RandomNumber;

import java.util.ArrayList;

/**
 * Created by Dell 6330 on 25/10/2016.
 */
public class Tetrimino extends Shape {

    public Tetrimino() {

    }

    public static Color tetriminoColor;
    public static int rotation = 1;

    private static ArrayList<Tetrimino> tetriminos = new ArrayList<>();

    public static ArrayList<Tetrimino> getTetriminos() {
        return tetriminos;
    }

    //Generates a new array of tetriminos with the size of a given range
    public static void generateArray(int range) {
        for (int i = 0; i < range; i++) {
            int randomNumber = RandomNumber.generate(0, 7);
            Tetrimino tetrimino = new Tetrimino();
            switch (randomNumber) {
                case 0:
                    tetrimino = new ShapeT();
                    break;
                case 1:
                    tetrimino = new ShapeO();
                    break;
                case 2:
                    tetrimino = new ShapeI();
                    break;
                case 3:
                    tetrimino = new ShapeS();
                    break;
                case 4:
                    tetrimino = new ShapeZ();
                    break;
                case 5:
                    tetrimino = new ShapeJ();
                    break;
                case 6:
                    tetrimino = new ShapeL();
                    break;
            }
            //If the same tetrimino is generated more than twice in a row, it does not save it to the array
            if (i > 1 && tetrimino.toString().equals(tetriminos.get(i - 1).toString()) &&
                    tetrimino.toString().equals(tetriminos.get(i - 2).toString())) {
                i--;
            } else {
                if (i > 9) { //If the same tetrimino has been generated twice in the last 8 passes, doesn't add to array
                    int repeated = 0;
                    for (int j = 1; j <= 8; j++) {
                        if (tetrimino.toString().equals(tetriminos.get(i - j).toString())) {
                            repeated++;
                        }
                        if (j == 8 && repeated >= 2) {
                            i--;
                            System.out.println("Too many");
                        } else if (j == 8 && repeated < 2) {
                            tetriminos.add(tetrimino);
                        }
                    }
                } else {
                    tetriminos.add(tetrimino);
                }
            }
        }
        int i = 0;
        while (i < 9) { //Remove the first 9 tetriminos, because they might be similar
            tetriminos.remove(0);
            i++;
        }
    }

    //Draws the currently active tetrimino
    public static void drawTetrimino() {
        switch (getTetriminos().get(0).toString()) {
            case "ShapeI":
                ShapeI.drawShape(Globals.playingField);
                ShapeI.setColor();
                break;
            case "ShapeJ":
                ShapeJ.drawShape(Globals.playingField);
                ShapeJ.setColor();
                break;
            case "ShapeL":
                ShapeL.drawShape(Globals.playingField);
                ShapeL.setColor();
                break;
            case "ShapeO":
                ShapeO.drawShape(Globals.playingField);
                ShapeO.setColor();
                break;
            case "ShapeS":
                ShapeS.drawShape(Globals.playingField);
                ShapeS.setColor();
                break;
            case "ShapeT":
                ShapeT.drawShape(Globals.playingField);
                ShapeT.setColor();
                break;
            case "ShapeZ":
                ShapeZ.drawShape(Globals.playingField);
                ShapeZ.setColor();
                break;
        }
    }

    //Chooses which Shape to rotate
    public static void rotateTetrimino() {
        switch (tetriminos.get(0).toString()) {
            case "ShapeI":
                ShapeI.rotateShape(Globals.playingField);
                break;
            case "ShapeJ":
                ShapeJ.rotateShape(Globals.playingField);
                break;
            case "ShapeL":
                ShapeL.rotateShape(Globals.playingField);
                break;
            case "ShapeO":
                break;
            case "ShapeS":
                ShapeS.rotateShape(Globals.playingField);
                break;
            case "ShapeT":
                ShapeT.rotateShape(Globals.playingField);
                break;
            case "ShapeZ":
                ShapeZ.rotateShape(Globals.playingField);
                break;
        }
    }

    public static Color getTetriminoColor() {
        return tetriminoColor;
    }

    public static void setTetriminoColor(Color tetriminoColor) {
        Tetrimino.tetriminoColor = tetriminoColor;
    }

    /**
     * @treatAsPrivate implementation detail
     * @deprecated This is an internal API that is not intended for use and will be removed in the next version
     */
    @Override
    public com.sun.javafx.geom.Shape impl_configShape() {
        return null;
    }

    @Override
    public String toString() {
        return ("Tetrimino class");
    }
}
