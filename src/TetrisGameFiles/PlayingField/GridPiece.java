package TetrisGameFiles.PlayingField;

import TetrisGameFiles.Resources.Globals;
import TetrisGameFiles.Tetriminos.Tetrimino;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Christopher Carr on 20/11/2016.
 */
public class GridPiece extends Rectangle {
    private boolean active;
    private boolean occupied;
    private Color savedColor;

    public void setSavedColor(Color savedColor) {
        this.savedColor = savedColor;
    }

    public Color getSavedColor() {
        return savedColor;
    }

    //GridPiece constructor
    public GridPiece(double x, double y, double width, double height, boolean active) {
        super(x, y, width, height);
        this.active = active;
        this.savedColor = null;
        setStroke(Color.GRAY);
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    //If gridpiece is active, fills it with color from tetrimino shape
    public void fillActive(Tetrimino current) {
        if (isActive()) {
            setFill(current.getTetriminoColor());
        }
    }

    //Checks to see if the gridpiece is occupied already
    public void checkOccupied() {
        if (isOccupied()) {
            setFill(this.savedColor);
        } else {
            setFill(this.savedColor);
        }
    }

    //Empties the gridpiece of active and occupied statuses
    public void emptyNode() {
        setActive(false);
        resetOccupied();
    }

    public boolean isActive() {
        return active;
    }

    //Changes active gridpieces to occupied gridpieces
    public static void lockActive() {
        System.out.println("Lock because bottom");
        for (int i = 0; i < Globals.GRID_COLUMNS; i++) {
            for (int j = 0; j < Globals.GRID_ROWS; j++) {
                if (Globals.playingField[i][j].isActive()) {
                    Globals.playingField[i][j].setActive(false);
                    Globals.playingField[i][j].setOccupied(true, Tetrimino.getTetriminos().get(0));
                }
            }
        }
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied, Tetrimino type) {
        this.occupied = occupied;
        this.savedColor = type.tetriminoColor;
    }

    public void setOccupied(boolean occupied, Color fill) {
        this.occupied = occupied;
        this.savedColor = fill;
    }

    public void resetOccupied() {
        this.occupied = false;
        this.savedColor = null;
    }
}
