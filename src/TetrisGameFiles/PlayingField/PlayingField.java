package TetrisGameFiles.PlayingField;

import TetrisGameFiles.Operations.Mechanics;
import TetrisGameFiles.Resources.Globals;
import TetrisGameFiles.Resources.PlayingPane;

/**
 * Created by Christopher Carr on 21/11/2016.
 */
public class PlayingField {

    private static int score = 0;

    public static void setScore(int score) {
        PlayingField.score = score;
    }

    //Generates the full playingfield of gridpieces
    public static GridPiece[][] generate(int Columns, int Rows, double Width, double Height, double XOffset,
                                         double YOffset) {
        GridPiece[][] playingField = new GridPiece[Columns][Rows];
        GridPiece node;
        for (int i = 0; i < Columns; i++) {
            for (int j = 0; j < Rows; j++) {
                //create new node
                node = new GridPiece((i * Width) + XOffset, (j * Height) + YOffset, Width, Height, false);

                //initialise as not occupied
                node.setActive(false);
                node.resetOccupied();

                //add to playingField array
                playingField[i][j] = node;
            }
        }
        return playingField;
    }

    //Function to redraw the whole field, to show changes in it
    public static void reDraw(int gridColumns, int gridRows, GridPiece[][] playingField) {
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                playingField[i][j].checkOccupied();
                playingField[i][j].fillActive(Globals.tetriminos.get(0));
            }
        }
    }

    //Empties the whole grid
    public static void wipeGrid(int gridColumns, int gridRows, GridPiece[][] playingField) {
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                playingField[i][j].emptyNode();
            }
        }
    }

    //removeFilledLines scans for full lines, removes them and shifts higher lines down, up to 4 at a time
    //If a line is removed, increases score and checks if the score is high enough for a speed increase
    public static void removeFilledLines(int gridColumns, int gridRows, GridPiece[][] playingField) {
        //Placeholders for row number of filled lines 1-4
        int fullLine1 = -1, fullLine2 = -1, fullLine3 = -1, fullLine4 = -1;
        for (int j = gridRows - 1; j >= 0; j--) {
            if (fullLine4 == -1) {
                for (int i = 0; i < gridColumns; i++) {
                    if (!playingField[i][j].isOccupied()) {
                        break;
                    }
                    if (i == (gridColumns - 1) && playingField[i][j].isOccupied()) {
                        if (fullLine1 > -1) {
                            if (fullLine2 > -1) {
                                if (fullLine3 > -1) {
                                    fullLine4 = j;
                                } else {
                                    fullLine3 = j;
                                }
                            } else {
                                fullLine2 = j;
                            }
                        } else {
                            fullLine1 = j;
                        }
                    }
                }
            }
        }
        if (fullLine4 > -1) {
            for (int j = fullLine4; j > 0; j--) {
                for (int i = 0; i < gridColumns; i++) {
                    if (Globals.playingField[i][j - 1].isOccupied()) {
                        Globals.playingField[i][j].setOccupied(true, playingField[i][j - 1].getSavedColor());
                        Globals.playingField[i][j].setSavedColor(Globals.playingField[i][j - 1].getSavedColor());
                    } else {
                        Globals.playingField[i][j].emptyNode();
                    }
                }
            }
            score++;
            if (score % 2 == 0) {
                Mechanics.timerChangeThisCycle = true;
                System.out.println("Speed increased");
            }
            for (int i = 0; i < gridColumns; i++) {
                Globals.playingField[i][0].resetOccupied();
            }
            System.out.println("Removed line 4");
        }
        if (fullLine3 > -1) {
            for (int j = fullLine3; j > 0; j--) {
                for (int i = 0; i < gridColumns; i++) {
                    if (Globals.playingField[i][j - 1].isOccupied()) {
                        Globals.playingField[i][j].setOccupied(true, playingField[i][j - 1].getSavedColor());
                        Globals.playingField[i][j].setSavedColor(Globals.playingField[i][j - 1].getSavedColor());
                    } else {
                        Globals.playingField[i][j].emptyNode();
                    }
                }
            }
            score++;
            if (score % 2 == 0) {
                Mechanics.timerChangeThisCycle = true;
                System.out.println("Speed increased");

            }
            for (int i = 0; i < gridColumns; i++) {
                Globals.playingField[i][0].resetOccupied();
            }
            System.out.println("Removed line 3");

        }
        if (fullLine2 > -1) {
            for (int j = fullLine2; j > 0; j--) {
                for (int i = 0; i < gridColumns; i++) {
                    if (Globals.playingField[i][j - 1].isOccupied()) {
                        Globals.playingField[i][j].setOccupied(true, playingField[i][j - 1].getSavedColor());
                        Globals.playingField[i][j].setSavedColor(Globals.playingField[i][j - 1].getSavedColor());
                    } else {
                        Globals.playingField[i][j].emptyNode();
                    }
                }
            }
            score++;
            if (score % 2 == 0) {
                Mechanics.timerChangeThisCycle = true;
                System.out.println("Speed increased");

            }
            for (int i = 0; i < gridColumns; i++) {
                Globals.playingField[i][0].resetOccupied();
            }
            System.out.println("Removed line 2");
        }
        if (fullLine1 > -1) {
            for (int j = fullLine1; j > 0; j--) {
                for (int i = 0; i < gridColumns; i++) {
                    if (Globals.playingField[i][j - 1].isOccupied()) {
                        Globals.playingField[i][j].setOccupied(true, playingField[i][j - 1].getSavedColor());
                        Globals.playingField[i][j].setSavedColor(Globals.playingField[i][j - 1].getSavedColor());
                    } else {
                        Globals.playingField[i][j].emptyNode();
                    }
                }
            }
            score++;
            if (score % 2 == 0) {
                Mechanics.timerChangeThisCycle = true;
                System.out.println("Speed increased");
            }
            for (int i = 0; i < gridColumns; i++) {
                Globals.playingField[i][0].resetOccupied();
            }
            System.out.println("Removed line 1");
        }
        PlayingPane.scoreText.setText(" Score: " + PlayingField.getScore());
        PlayingField.reDraw(Globals.GRID_COLUMNS, Globals.GRID_ROWS, Globals.playingField);
    }

    public static int getScore() {
        return score;
    }
}
