package TetrisGameFiles.Operations;

import TetrisGameFiles.PlayingField.GridPiece;

/**
 * Created by Christopher Carr on 21/11/2016.
 */
public class Move {
    //These are the move functions, for left, right and down. All check whether the move is possible.
    public static void left(GridPiece[][] playingField, int gridColumns, int gridRows) {
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                if (i < 9) {
                    if (playingField[i][j].isOccupied() && playingField[i + 1][j].isActive()) {
                        return;
                    }
                }
            }
        }
        for (int i = 0; i < gridColumns; i++) {
            for (int j = 0; j < gridRows; j++) {
                if (playingField[i][j].isActive()) {
                    if (i == 0) {
                        return;
                    } else {
                        playingField[i - 1][j].setActive(true);
                    }
                    if (i == gridColumns - 1) {
                        playingField[i][j].setActive(false);
                    } else if (playingField[i + 1][j].isActive()) {
                        playingField[i][j].setActive(true);
                    } else {
                        playingField[i][j].setActive(false);
                    }
                }
            }
        }
    }

    public static void right(GridPiece[][] playingField, int gridColumns, int gridRows) {
        for (int i = gridColumns - 1; i >= 0; i--) {
            for (int j = 0; j < gridRows; j++) {
                if (i > 0) {
                    if (playingField[i][j].isOccupied() && playingField[i - 1][j].isActive()) {
                        return;
                    }
                }
            }
        }
        for (int i = gridColumns - 1; i >= 0; i--) {
            for (int j = 0; j < gridRows; j++) {
                if (playingField[i][j].isActive()) {
                    if (i == gridColumns - 1) {
                        return;
                    } else {
                        playingField[i + 1][j].setActive(true);
                    }
                    if (i == 0) {
                        playingField[i][j].setActive(false);
                    } else if (playingField[i - 1][j].isActive()) {
                        playingField[i][j].setActive(true);
                    } else {
                        playingField[i][j].setActive(false);
                    }
                }
            }
        }
    }

    public static void down(GridPiece[][] playingField, int gridColumns, int gridRows) {
        for (int i = 0; i < gridColumns; i++) {
            for (int j = gridRows - 1; j >= 0; j--) {
                if (playingField[i][j].isOccupied() && playingField[i][j - 1].isActive()) {
                    Mechanics.isStuck = true;
                    return;
                } else {
                    Mechanics.isStuck = false;
                }
            }
        }
        for (int j = gridRows - 1; j >= 0; j--) {
            for (int i = 0; i < gridColumns; i++) {
                if (playingField[i][j].isActive()) {
                    if (j == 21) {
                        Mechanics.atBottom = true;
                        return;
                    } else {
                        playingField[i][j + 1].setActive(true);
                    }
                    if (j == 0) {
                        playingField[i][j].setActive(false);
                    } else if (playingField[i][j - 1].isActive()) {
                        playingField[i][j].setActive(true);
                    } else {
                        playingField[i][j].setActive(false);
                    }
                }
            }
        }
    }
}
