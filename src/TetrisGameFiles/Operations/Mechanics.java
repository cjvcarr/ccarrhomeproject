package TetrisGameFiles.Operations;

import TetrisApp.TetrisGame;
import TetrisGameFiles.PlayingField.GridPiece;
import TetrisGameFiles.PlayingField.PlayingField;
import TetrisGameFiles.Resources.Globals;
import TetrisGameFiles.Resources.PlayingPane;
import TetrisGameFiles.Tetriminos.Tetrimino;
import javafx.scene.paint.Color;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Christopher Carr on 22/11/2016.
 */
public class Mechanics {
    public static boolean timerChangeThisCycle;
    public static boolean lockNextCycle = false;
    public static boolean lockAfterTwoCycles = false;
    public static boolean isStuck;
    public static boolean atBottom;
    public static boolean resetTimerDelay;
    public static int timerDelay = 1000;

    //Tickdown timer for the game cycles
    public static void tickDown() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (!TetrisGame.paused && !TetrisGame.gameEnded) { //This code only runs if the game is active
                    Move.down(Globals.playingField, Globals.GRID_COLUMNS, Globals.GRID_ROWS);
                    PlayingField.reDraw(Globals.GRID_COLUMNS, Globals.GRID_ROWS,
                            Globals.playingField);
                    if (lockAfterTwoCycles) {
                        if (isStuck) {
                            lockNextCycle = true;
                            lockAfterTwoCycles = false;
                        }
                    }
                    if (lockNextCycle) { //This locks the piece if lockNextCycle is active and it is still stuck
                        if (atBottom || isStuck) {
                            GridPiece.lockActive();
                            Globals.tetriminos.remove(0);
                            PlayingField.removeFilledLines(Globals.GRID_COLUMNS, Globals.GRID_ROWS, Globals.playingField);
                            atBottom = false;
                            isStuck = false;
                            lockNextCycle = false;
                            for (int i = 0; i < Globals.GRID_COLUMNS; i++) {
                                if (Globals.playingField[i][2].isOccupied()) {
                                    endGame();
                                }
                            }
                            if (!TetrisGame.gameEnded) {
                                Tetrimino.drawTetrimino();
                            }
                        } else { //If it isn't at the bottom or stuck, it will reset the locking cycle
                            atBottom = false;
                            isStuck = false;
                            lockNextCycle = false;
                        }
                    } else if (atBottom ) { //If tetrimino is at the bottom, it will lock after 1 cycle
                        lockNextCycle = true;
                    } else if (isStuck) {
                        lockAfterTwoCycles = true; //If tetrimino is stuck, it will lock after 2 cycles
                    }
                    if (Mechanics.timerChangeThisCycle && timerDelay > 200) { //This changes the timer speed
                        timer.cancel();
                        timerDelay = timerDelay - 100;
                        tickDown();
                        timerChangeThisCycle = false;
                    }
                    if (resetTimerDelay) { //This resets the timer speed for a new game
                        timer.cancel();
                        timerDelay = 1000;
                        tickDown();
                        resetTimerDelay = false;
                    }
                }
            }
        }, 0, timerDelay);
    }

    //All functions for setting up a new game
    public static void setupNewGame() {
        Tetrimino.generateArray(1009);
        PlayingPane.endGameText.setFill(null);
        PlayingField.setScore(0);
        PlayingPane.scoreText.setText(" Score: " + PlayingField.getScore());
        PlayingField.wipeGrid(Globals.GRID_COLUMNS, Globals.GRID_ROWS, Globals.playingField);
        PlayingField.reDraw(Globals.GRID_COLUMNS, Globals.GRID_ROWS, Globals.playingField);
        resetTimerDelay = true;
        TetrisGame.paused = false;
        TetrisGame.gameEnded = false;
        Tetrimino.drawTetrimino();
    }

    //Functions for when a game has ended
    public static void endGame() {
        TetrisGame.gameEnded = true;
        PlayingPane.endGameText.setFill(Color.BLACK);
        System.out.println("Ended the game");
    }
}
