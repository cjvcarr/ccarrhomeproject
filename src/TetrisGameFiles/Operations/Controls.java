package TetrisGameFiles.Operations;

import TetrisApp.TetrisGame;
import TetrisGameFiles.PlayingField.PlayingField;
import TetrisGameFiles.Resources.Globals;
import TetrisGameFiles.Resources.PlayingPane;
import TetrisGameFiles.Tetriminos.Tetrimino;
import javafx.scene.input.KeyEvent;

/**
 * Created by Christopher Carr on 07/12/2016.
 */
public class Controls {

    //Sets user controls, to be started at first newGame. When game is paused or ended, controls do not work
    public static void userControls() {
        PlayingPane.gamePane.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            switch (event.getCode()) {
                case LEFT:
                    event.consume();
                    if (!TetrisGame.paused) {
                        Move.left(Globals.playingField, Globals.GRID_COLUMNS, Globals.GRID_ROWS);
                        PlayingField.reDraw(Globals.GRID_COLUMNS, Globals.GRID_ROWS, Globals.playingField);
                    }
                    break;
                case RIGHT:
                    event.consume();
                    if (!TetrisGame.paused) {
                        Move.right(Globals.playingField, Globals.GRID_COLUMNS, Globals.GRID_ROWS);
                        PlayingField.reDraw(Globals.GRID_COLUMNS, Globals.GRID_ROWS, Globals.playingField);
                    }
                    break;
                case DOWN:
                    event.consume();
                    if (!TetrisGame.paused) {
                        Move.down(Globals.playingField, Globals.GRID_COLUMNS, Globals.GRID_ROWS);
                        PlayingField.reDraw(Globals.GRID_COLUMNS, Globals.GRID_ROWS, Globals.playingField);
                    }
                    break;
                case UP:
                    event.consume();
                    if (!TetrisGame.paused) {
                        Tetrimino.rotateTetrimino();
                        PlayingField.reDraw(Globals.GRID_COLUMNS, Globals.GRID_ROWS, Globals.playingField);
                    }
                    break;
                case P:
                    event.consume();
                    if (!TetrisGame.paused) {
                        TetrisGame.paused = true;
                    } else if (TetrisGame.paused) {
                        TetrisGame.paused = false;
                    }
            }
        });

    }
}

