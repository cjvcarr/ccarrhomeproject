package TetrisApp;

import TetrisGameFiles.Resources.NewGame;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.text.Text;
import javafx.stage.Stage;


/**
 * Created by Dell 6330 on 25/10/2016.
 */
public class TetrisGame extends Application {
    public static boolean gameStageActive, playingPaneOpen; //Variables to determine if the playing window is in use
    public static boolean paused = false, gameEnded = false; //Variables to determine the state of the current game

    //This will open the main meni when the program is started
    @Override
    public void start(Stage mainMenuStage) throws Exception {
        //Variables to setup the height and width of the mainMenu. since they are only used here, they can be local
        int mainMenuWidth = 500;
        int mainMenuHeight = 400;

        //Set the title, text and buttons
        mainMenuStage.setTitle("Christopher Carr CSE C11 Tetris");
        Text creatorText = new Text("Totally not ripped off by Christopher Carr, CSE C11");
        creatorText.setFont(Font.font("Stencil", 15));
        Pane mainMenuPane = new Pane();
        Button newGameButton = new Button("New game");
        creatorText.setFontSmoothingType(FontSmoothingType.LCD);
        Button quitButton = new Button("Quit");
        quitButton.setPrefWidth(140);
        quitButton.setFont(Font.font("Stencil", 22));
        newGameButton.setPrefWidth(140);
        newGameButton.setFont(Font.font("Stencil", 22));

        //Load the tetris image and set display parameters
        Image tetrisLogo = new Image("/tetrislogo.png");
        ImageView tetrisLogoView = new ImageView();
        tetrisLogoView.setImage(tetrisLogo);
        tetrisLogoView.setX(mainMenuWidth / 2 - 200);
        tetrisLogoView.setY(0);

        //Set placement of elements that are dependent of the image
        creatorText.setY(tetrisLogo.getHeight() + 15);
        creatorText.setX(mainMenuWidth / 2 - 210);
        HBox hbButtons = new HBox();
        hbButtons.setLayoutX(mainMenuWidth / 2 - 150);
        hbButtons.setLayoutY(tetrisLogo.getHeight() + 30);
        hbButtons.setSpacing(20);
        hbButtons.getChildren().addAll(newGameButton, quitButton);

        //Add all children to the mainMenuPane and show the scene
        mainMenuPane.getChildren().addAll(hbButtons, tetrisLogoView, creatorText);
        Scene mainMenuScene = new Scene(mainMenuPane, mainMenuWidth, mainMenuHeight, Color.WHITE);
        mainMenuStage.setScene(mainMenuScene);
        mainMenuStage.show();

        //Set button actions
        newGameButton.setOnAction(event -> {
            if (!gameStageActive) {
                NewGame.openGameStage();
                gameStageActive = true;
                mainMenuStage.hide();
            }
        });
        quitButton.setOnAction(e -> System.exit(0));
        mainMenuStage.setOnCloseRequest(e -> System.exit(0));
    }

    public static void main(String[] args) {
        launch(args);
    }

}
