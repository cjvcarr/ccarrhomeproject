package lib;

/**
 * Created by Dell 6330 on 25/10/2016.
 */
public class RandomNumber {

    public static int generate(int min, int max) {
        int range = max - min;
        int random = (int) (Math.random() * (range)) + min;
        return random;
    }
}